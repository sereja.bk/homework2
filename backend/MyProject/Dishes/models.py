from django.db import models
from django.utils.timezone import now

# Create your models here.
class Cook(models.Model):

    fio = models.CharField(
        verbose_name = 'ФИО повара',
        max_length = 80,
        null = False,
        blank = False
        )

    birthday = models.DateField(
        verbose_name = 'Дата рождения',
        null = False,
        blank = False
        )

    age = models.IntegerField(
        verbose_name = 'Возраст',
        default = None
        )

    phone = models.CharField(
        verbose_name = 'Телефон',
        max_length = 12,
        default = None
        )

    def __str__(self):
        return self.fio

class Dish(models.Model):

    title = models.CharField(
        verbose_name = 'Название',
        max_length = 20,
        default = 'Без названия'
        )

    time = models.TimeField(
        verbose_name = 'Длительность готовки',
        default = None
        )

    cooker = models.ForeignKey('Cook', on_delete = models.PROTECT)

    def __str__(self):
        return self.title

class Ingredients(models.Model):

    title = models.CharField(
        verbose_name = 'Название',
        max_length = 30,
        null = False,
        blank = False
        )

    exp_date_days = models.IntegerField(
        verbose_name = 'Срок годности в днях',
        default = None
        )

    dish = models.ForeignKey('Dish', on_delete = models.PROTECT)

    def __str__(self):
        return f"{self.dish} - {self.title}"
